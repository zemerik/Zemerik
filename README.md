<p align="center"> <img src="https://komarev.com/ghpvc/?username=zemerik&label=Profile%20views&color=0e75b6&style=flat" alt="zemerik" /> </p>

<p align = "center">
<a href="https://twitter.com/zemerik_x" target="_blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/twitter.svg" alt="zemerik_x" height="30" width="40" /></a>
<a href="https://linkedin.com/in/zemerik" target="_blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="zemerik" height="30" width="40" /></a>
<a href="https://instagram.com/zemerik_insta" target="_blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/instagram.svg" alt="zemerik_insta" height="30" width="40" /></a>
<a href="https://discord.gg/WMCEHzwkup" target="_blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/discord.svg" alt="WMCEHzwkup" height="30" width="40" /></a>
</p>

<p align = "center"><img src = "assets/ironman-head.png"></p>

<div align = "center">
  <details close>
    <summary>
      Hemang Yadav
    </summary>

![Banner](assets/banner_github.png)

<details close>
  <summary>
    About me:
  </summary>

![About](assets/About2.gif)

</details>

<details>
  <summary>
    Tech Stack
  </summary>

<br>

![Tech Stack](assets/tools.svg)

</details>

<details>
  <summary>
    My Stats:
  </summary>

![Github Stats](https://github-readme-stats.vercel.app/api?username=zemerik&show_icons=true&theme=cobalt&locale=en)  

</details>

<details>
  <summary>
    Current Status:
  </summary>

![Status](https://lanyard-profile-readme.vercel.app/api/1018816958587748383?showDisplayName=true&bg=141321&idleMessage=Let%27s%20Chat%20on%20Discord)

</details>

</details>
</div>

<p align = "center"><img src = "assets/ironman-legs.png"></p>

